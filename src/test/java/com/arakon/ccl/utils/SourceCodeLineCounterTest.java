package com.arakon.ccl.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SourceCodeLineCounterTest {

    @Test
    public void withoutComments() throws IOException {
        final BufferedReader bufferedReader = readFile("dir/Test.java");
        final int numberOfLines = SourceCodeLineCounter.getNumberOfLines(bufferedReader);
        Assertions.assertEquals(5, numberOfLines);
    }

    @Test
    public void withSimpleComments() throws IOException {
        final BufferedReader bufferedReader = readFile("dir/Test2.java");
        final int numberOfLines = SourceCodeLineCounter.getNumberOfLines(bufferedReader);
        Assertions.assertEquals(5, numberOfLines);
    }

    @Test
    public void withMoreComplicatedComments() throws IOException {
        final BufferedReader bufferedReader = readFile("dir/Test3.java");
        final int numberOfLines = SourceCodeLineCounter.getNumberOfLines(bufferedReader);
        Assertions.assertEquals(7, numberOfLines);
    }

    @Test
    public void withPathologicalComments() throws IOException {
        final BufferedReader bufferedReader = readFile("dir/Test4.java");
        final int numberOfLines = SourceCodeLineCounter.getNumberOfLines(bufferedReader);
        Assertions.assertEquals(7, numberOfLines);
    }

    private BufferedReader readFile(String path) {
        final InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(path);
        return new BufferedReader(new InputStreamReader(resourceAsStream));
    }

}
