package com.arakon.ccl.utils;

import java.io.File;

public class ErrorPrinterUtil {

    //simple comment should be ignored
    /*
        such comment should be ignored as well
     */
    public static final String WRONG_ARGUMENT_ERROR_MESSAGE = "Wrong argument! Please, provide valid single Java file or folder path!";

}