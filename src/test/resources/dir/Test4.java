package com.arakon.ccl.utils;

import java.io.File;

public class ErrorPrinterUtil {

    /******
     This is a test program with 5 lines of code
     *  \/* no nesting allowed!
     //*****//***/// Slightly pathological comment ending...

    //simple comment should be ignored
    /*
        such comment should be ignored as well
     */
    public static final String WRONG_ARGUMENT_ERROR_MESSAGE = "Wrong argument! Please, provide valid single Java file or folder path!";

    // the comments below should be in count!
    /* comment */ public static int i = 0;
    public static int i = 0; // still in count
}