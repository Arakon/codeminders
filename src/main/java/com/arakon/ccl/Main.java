package com.arakon.ccl;

import com.arakon.ccl.utils.SourceCodeLineCounter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static final String WRONG_ARGUMENT_ERROR_MESSAGE = "Wrong argument! Please, provide valid single Java file or folder path!";
    public static final String TOTAL_COUNT_PLACEHOLDER = "%%TOTAL_COUNT_PLACEHOLDER%%";
    public static final String SUBFOLDER_COUNT_PLACEHOLDER = "%%SUBFOLDER_COUNT_PLACEHOLDER%%";
    public static final String JAVA_SUFFIX = ".java";
    public static final String TAB = "\t";
    public static final char RETURN_CHAR = '\n';
    public static final String ROOT = "root : ";

    public static void main(String[] args) {
        System.out.println(doJob(args));
    }

    private static String doJob(String[] args) {
        final String result;

        if (args.length != 1) {
            result = WRONG_ARGUMENT_ERROR_MESSAGE;
            return result;
        }
        final String path = args[0];
        if (path == null || path.length() == 0) {
            result = WRONG_ARGUMENT_ERROR_MESSAGE;
            return result;
        }
        final File inputFile = new File(path);
        if (!inputFile.exists()) {
            result = WRONG_ARGUMENT_ERROR_MESSAGE;
            return result;
        }

        if (inputFile.isDirectory()) {
            result = processDirectory(inputFile);
        } else {
            result = processFile(path, inputFile);
        }
        return result;
    }

    private static String processFile(String path, File inputFile) {
        if (path.endsWith(JAVA_SUFFIX)) {
            final int i = countLinesInFile(inputFile);
            return formatFileOutput(inputFile, i);
        } else {
            return WRONG_ARGUMENT_ERROR_MESSAGE;
        }
    }

    private static String processDirectory(File inputFile) {
        final File[] files = inputFile.listFiles();
        if (files != null) {
            final StringBuilder sb = new StringBuilder();
            int totalCount = 0;

            // we don't know the total count before we count it. Put the placeholder here.
            sb.append(ROOT).append(TOTAL_COUNT_PLACEHOLDER);
            for (File file : files) {
                totalCount += iterateEntries(file, sb, TAB);
            }
            return sb.toString().replace(TOTAL_COUNT_PLACEHOLDER, String.valueOf(totalCount));
        } else {
            return ROOT + "0";
        }
    }

    /*
     * Recursively iterates over all directory entries, counts all valid lines and append the output to the given builder
     */
    private static int iterateEntries(
            File file,
            StringBuilder sb,
            String tab
    ) {
        int totalCount = 0;

        // if folder - recursively iterates it
        if (file.isDirectory()) {

            // we don't know how much lines in the folder right now, so let's place the placeholder here
            // and then change it with the real number
            final String result = formatFileOutput(file, SUBFOLDER_COUNT_PLACEHOLDER);
            sb.append(RETURN_CHAR).append(tab).append(result);
            final File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    totalCount += iterateEntries(f, sb, tab.concat(TAB));
                }
            }

            // change the placeholder with counted number
            final int start = sb.indexOf(SUBFOLDER_COUNT_PLACEHOLDER);
            sb.replace(start, start + SUBFOLDER_COUNT_PLACEHOLDER.length(), String.valueOf(totalCount));

            // if the file - just count the number of lines and format the output
        } else {
            final int i = countLinesInFile(file);
            sb.append(RETURN_CHAR).append(tab).append(formatFileOutput(file, i));
            totalCount = i;
        }
        return totalCount;
    }

    private static int countLinesInFile(File inputFile) {
        try {
            final BufferedReader fileReader = new BufferedReader(new FileReader(inputFile));
            return SourceCodeLineCounter.getNumberOfLines(fileReader);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String formatFileOutput(File inputFile, String i) {
        return String.format("%s : %s", inputFile.getName(), i);
    }

    public static String formatFileOutput(File inputFile, int i) {
        return formatFileOutput(inputFile, String.valueOf(i));
    }
}
